#How to use
This utility is simple enough to use, just use JQuery to select the form element (for example, $('#myForm')),
then call the FormMaster function ( $('#myForm').MasterThisForm() ). In the future, there will be arguments available to this function, but
for now the only customization is found by either hacking the utility (which I openly agree with!), or using the input attributes, found below.

#input attributes
These attributes are there to make your life easier. Add the attributes to the form you wish to check, and FormMaster will take these into consideration

##fm-minlength
Pretty self explanatory, this attribute gives a minimum length of data

##fm-maxlength
Again, this attribute gives a maximum length of data

##fm-regexmatch
This attribute is a handy little way of ensuring that the data matches your requirements, by matching it against the RegEx
expression in this attribute