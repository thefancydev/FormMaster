Element.prototype.MasterThisForm = function(){
  if (!$(this).is('form')) {
    throw 'The element called must be a form type'
  }
  else {
    $(this).children().filter(function(){
      return $(this).is('input');
    }.each(){
      //$(this) is now the input children of the form
      
      var minlen = $(this).attr('fm-minlength');
      var maxlen = $(this).attr('fm-maxlength');
      var regexm = $(this).attr('fm-regexmatch');
      
      $(this).change(function(){
        if (typeof minlen !== typeof undefined && minlen !== false) {
          if ($(this).val().length < minlen) {
            $(this).after('<div class="incorrectInput">This field does not match the required minimum length of' + minlen + '</div>');
          }
        }
        
        if (typeof maxlen !== typeof undefined && maxlen !== false) {
          if ($(this).val().length > maxlen) {
            $(this).after('<div class="incorrectInput">This field does not match the required maximum length of' + maxlen + '</div>');
          }
        }
        
        if (typeof regexm !== typeof undefined && regexm !== false) {
          if (!$(this).val().match(regexm)) {
            $(this).after('<div class="incorrectInput">This field does not match the required input</div>');
          }
        }
      })
    }
  }
}